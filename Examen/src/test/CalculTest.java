import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
  
   
   /**
    * Tests de la classe Calcul.
    */
   public class CalculTest {
   
    @Test
   public void testLongueurTotal() {      
        assertEquals(2, new Calcul(new String[]{"a", "b"}).longueurTotale());
    }
        
    @Test
    public void testLongueurTotalException() {
        try {
        new Calcul(null).longueurTotale();
        fail("exception non levée");
        } catch(IllegalArgumentException e) {
        // ok
        }
    }
  
    @Test
    public void testNomLePlusLong() {      
        assertEquals("bcd", new Calcul(new String[]{"a", "bcd", "ze"}).NOM_LE_PLUS_LONG());
    }
      
}