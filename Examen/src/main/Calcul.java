/**
* Une classe.
*/
public class Calcul {

   /**
    * un tableau de chaînes de carcatères représentant des 'noms'.
    */
   private final String[] noms;

  /**
   * Constructeur
   */
  public Calcul(String[] noms) {
      this.noms = noms;
  }

  /**
   * Une méthode qui renvoie la somme des longueurs des noms si elle est inférieure à 255, 255 sinon.
   * @throws IllegalArgumentException si le tableau est null.
   * @return somme des longueurs des 'noms'.
   */
  public  int longueurTotale() {
	String stringTotal = "";
	for (int i = 0; i < noms.length; i++) {
	    stringTotal = stringTotal + noms[i];
	}
	if ( stringTotal.length() > 255 ) {
	    return 255;
	}
	return stringTotal.length();
  }

  /**
   * @return le nom le plus long, null si le tableau est vide.
   * @throws IllegalStateException si le tableau est null
   */
  public String NOM_LE_PLUS_LONG() {
	if (noms.length == 0) {
	    return null;
	}
	if (noms == null) {
	    throw new IllegalStateException("tableau null");
	}

	String result = "";
    int longueur = 0;
	for(int i = 0; i < noms.length; i++) {
	    if (noms[i].length() > result.length()) {
    		result = noms[i];
    		longueur = result.length();
	    }
	}
	return result;
  }

}