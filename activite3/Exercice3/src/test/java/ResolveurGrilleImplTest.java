// import pour les methodes statique Assert.
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;


// import pour la methode statique Test.
import org.junit.jupiter.api.*;

/**
 * Tests unitaire pour la classe Calcul.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public class ResolveurGrilleImplTest {

    /**
    * Variable NUM1 entier.
    */
    public static final int NUM1 = 1;
    /**
    * Variable NUM2 entier.
    */
    public static final int NUM2 = 2;
    /**
    * Variable NUM3 entier.
    */
    public static final int NUM3 = 3;

    /**
    * Variable NUM4 entier.
    */
    public static final int NUM4 = 4;

    /**
    * Caractere de case vide
    */
    public static final char EMPTY = '@';

    /**
    * Caractere de test saisie 5
    */
    public static final char CHAR5 = '5';

    /**
    * Caractere de test saisie G
    */
    public static final char CHARG = 'g';

    /**
    * Variable NUM4 entier.
    */
    public static final int NUM18 = 18;

    /**
    * Variable NUMCHECK1 entier.
    */
    public static final int NUMCHECK1 = 16;
    /**
    * Variable NUMCHECK2 entier.
    */
    public static final int NUMCHECK2 = 9;
    /**
    * Variable NUMCHECK2 entier.
    */
    public static final int NUMCHECK3 = 25;

    public static ResolveurGrille resgrille;

    /**
    * Test unitaire du Constructeur().
    */
    @BeforeAll
    public static void testConstructeur() {
        GrilleImpl testGrille = new GrilleImpl(NUMCHECK1);
        resgrille = new ResolveurGrilleImpl(testGrille);
    }

    /**
    * Test unitaire de la methode somme().
    */
    @Test
    public final void testVerifLigne() {
        GrilleImpl testGrille = new GrilleImpl(NUMCHECK1);
        ResolveurGrille resgrille = new ResolveurGrilleImpl(testGrille);

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            resgrille.verifLigne(NUM18, CHAR5);
        });
        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        testGrille.setValue(NUM3,NUM2,CHAR5);
        assertTrue(resgrille.verifLigne(NUM2,CHAR5));

        testGrille.setValue(NUM2,NUM3,CHAR5);
        assertFalse(resgrille.verifLigne(NUM2,CHAR5));
    }

    /**
    * Test unitaire de la methode somme().
    */
    @Test
    public final void testVerifColonne() {
        GrilleImpl testGrille = new GrilleImpl(NUMCHECK1);
        ResolveurGrille resgrille = new ResolveurGrilleImpl(testGrille);


        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            resgrille.verifColonne(NUM18, CHAR5);
        });
        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        testGrille.setValue(NUM3,NUM2,CHAR5);
        assertFalse(resgrille.verifColonne(NUM2,CHAR5));

        testGrille.setValue(NUM2,NUM3,CHAR5);
        assertTrue(resgrille.verifColonne(NUM1,CHAR5));
    }

    /**
    * Test unitaire de la methode verifBlock().
    */
    @Test
    public final void testVerifBlock() {
        GrilleImpl testGrille = new GrilleImpl(NUMCHECK1);
        ResolveurGrille resgrille = new ResolveurGrilleImpl(testGrille);

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            resgrille.verifBlock(NUM1,NUM18, CHAR5);
        });
        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        testGrille.setValue(NUM2,NUM2,CHAR5);
        assertFalse(resgrille.verifBlock(NUM1,NUM1,CHAR5));

        testGrille.setValue(NUM3,NUM3,CHAR5);
        assertFalse(resgrille.verifBlock(NUM4,NUM4,CHAR5));

    }

    /**
    * Test unitaire de la methode solveur().
    */
    @Test
    public final void testSolveur(){
        GrilleImpl testGrille9 = new GrilleImpl(NUMCHECK2);
        GrilleParser testGrilleParser9 = new GrilleParser();
        File fichier9=null;

        try {
            fichier9 = new File("sudoku-9x9.txt");
            System.out.println("le fichier "+fichier9.getName()+(fichier9.exists()?" existe":" n'existe pas"));
            testGrilleParser9.parse(fichier9,testGrille9);
        }catch(java.io.IOException e){
            System.out.println("Something went wrong.");
        }
        testGrille9.display();
        ResolveurGrille resgrille9 = new ResolveurGrilleImpl(testGrille9);
        resgrille9.solveur();
        testGrille9.display();

        GrilleImpl testGrille16 = new GrilleImpl(NUMCHECK1);
        GrilleParser testGrilleParser16 = new GrilleParser();
        File fichier16=null;

        try {
            fichier16 = new File("sudoku-16x16.txt");
            System.out.println("le fichier "+fichier16.getName()+(fichier16.exists()?" existe":" n'existe pas"));
            testGrilleParser16.parse(fichier16,testGrille9);
        }catch(java.io.IOException e){
            System.out.println("Something went wrong.");
        }
        testGrille16.display();
        ResolveurGrille resgrille16 = new ResolveurGrilleImpl(testGrille16);
        resgrille16.solveur();
        testGrille16.display();

        GrilleImpl testGrille25 = new GrilleImpl(NUMCHECK3);
        GrilleParser testGrilleParser25 = new GrilleParser();
        File fichier25=null;

        try {
            fichier25 = new File("sudoku-25x25.txt");
            System.out.println("le fichier "+fichier25.getName()+(fichier25.exists()?" existe":" n'existe pas"));
            testGrilleParser16.parse(fichier25,testGrille9);
        }catch(java.io.IOException e){
            System.out.println("Something went wrong.");
        }
        testGrille25.display();
        ResolveurGrille resgrille25 = new ResolveurGrilleImpl(testGrille25);
        resgrille25.solveur();
        testGrille25.display();

        assertFalse(testGrille9.complete());
        assertFalse(testGrille16.complete());
        assertFalse(testGrille25.complete());
    }

}
