/**
* Implementation classe de resolution grille.
* @author maxime.ngoe@gmail.com (Maxime NGOE)
**/

/**
 *Implémentation Interface Resolveur Grille de sudoku.
 */
public class ResolveurGrilleImpl implements ResolveurGrille {

  /**
  * Grille à résoudre.
  */
  private final GrilleImpl myGrille;

  /**
  * Taille de la sous grille.
  */
  static final int SOUSGRILLESIZE = 3;

 /** Constructeur de la classe.
  * @param myGrilleImpl
  *            valeur a mettre dans la grille
  */
  protected ResolveurGrilleImpl(final GrilleImpl myGrilleImpl) {
      this.myGrille = myGrilleImpl;
  }

    /**
    * Vérifie une valeur par ligne dans la grille.
    *
    * @param x       position x dans la grille
    * @param value
    *            valeur a mettre dans la case
    * @return true si la ligne comprends deja la valeur
    * @throws IllegalArgumentException si x est hors bornes
    */
    public final boolean verifLigne(final int x, final char value) {
      if (x > this.myGrille.getDimension()) {
        throw new IllegalArgumentException("Parametre Hors Borne");
      }
      for (int j = 0; j < this.myGrille.getDimension(); j++) {
          if (this.myGrille.getValue(x, j) == value) {
              return false;
          }
      }
      return true;
    }

    /**
    * Test si la valeur est présente dans la colonne.
    *
    * @param y      position y dans la grille
    * @param value
    *            valeur a mettre dans la case
    * @return true si la colonne comprends deja la valeur
    * @throws IllegalArgumentException si y est hors bornes
    */
    public final boolean verifColonne(final int y, final char value) {
        if (y > this.myGrille.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        for (int j = 0; j < this.myGrille.getDimension(); j++) {
          if (this.myGrille.getValue(j, y) == value) {
                return false;
          }
        }
        return true;
    }

    /**
    * Recupere une valeur de la grille par block.
    *
    * @param xverif      position x dans la grille
    * @param yverif      position y dans la grille
    * @param value
    *            valeur a mettre dans la case
    * @return true si la valeur n'est pas présente dans le sous élément 3x3
    * @throws IllegalArgumentException si x ou y sont hors bornes
    */
    public final boolean verifBlock(final int xverif,
      final int yverif, final char value) {
        int x = xverif;
        int y = yverif;
        if (x > this.myGrille.getDimension()
          || y > this.myGrille.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        int i = x - (x % this.SOUSGRILLESIZE);
        int j = y - (y % this.SOUSGRILLESIZE);
        int iMax = 0;
        int jMax = 0;
        if (i + this.SOUSGRILLESIZE > this.myGrille.getDimension()) {
          iMax = this.myGrille.getDimension();
        } else {
          iMax = i + this.SOUSGRILLESIZE;
        }
        if (j + this.SOUSGRILLESIZE > this.myGrille.getDimension()) {
          jMax = this.myGrille.getDimension();
        } else {
          jMax = j + this.SOUSGRILLESIZE;
        }
        for (x = i; x < iMax; x++) {
            for (y = j; y < jMax; y++) {
                if (this.myGrille.getValue(x, y) == value) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
    * Solveur du Sudoku.
    *
    * @return true si la grille est complete
    */
    public final boolean solveur() {
      for (int i = 0; i < this.myGrille.getDimension(); i++) {
        for (int j = 0; j < this.myGrille.getDimension(); j++) {
          if (this.myGrille.getValue(i, j) == '@') {
            for (char possible:this.myGrille.possible) {
              if (this.verifLigne(i, possible)
                && this.verifColonne(j, possible)
                && this.verifBlock(i, j, possible)) {
                    this.myGrille.setValue(i, j, possible);
                    this.solveur();
                    return this.myGrille.complete();
                }
              }
          }
        }
      }
    return false;
    }
}
