/**
 * Interface d'une grille de sudoku.
 */
public interface ResolveurGrille {

 /**
  * Vérifie une valeur par ligne dans la grille.
  *
  * @param x       position x dans la grille
  * @param value
  *            valeur a mettre dans la case
  * @return true si la ligne comprends deja la valeur
  * @throws IllegalArgumentException si x est hors bornes
  */
 boolean verifLigne(int x, char value) throws IllegalArgumentException;
 /**
  * Recupere une valeur par colonne de la grille.
  *
  * @param y      position y dans la grille
  * @param value
  *            valeur a mettre dans la case
  * @return true si la colonne comprends deja la valeur
  * @throws IllegalArgumentException si y est hors bornes
  */
 boolean verifColonne(int y, char value) throws IllegalArgumentException;
 /**
  * Recupere une valeur de la grille par block.
  *
  * @param x      position x dans la grille
  * @param y      position y dans la grille
  * @param value
  *            valeur a mettre dans la case
  * @return true si la valeur n'est pas présente dans le sous élément 3x3
  * @throws IllegalArgumentException si x ou y sont hors bornes
  */
 boolean verifBlock(int x, int y, char value) throws IllegalArgumentException;

  /**
  * Solveur du Sudoku.
  *
  * @return true si la grille est complete
  */
 boolean solveur();

}
