/**
* Implementation classe de grille.
* @author maxime.ngoe@gmail.com (Maxime NGOE)
**/

/**
 *Implementation Interface Grille de sudoku.
 */
public class GrilleImpl implements Grille {

  /**
  * Taille de la grille.
  */
  static int GRILLESIZE = 16;

  /**
  * Initiation du tableau des valeurs possible.
  */
  static char[] possible = new char[9];
  /**
  * Taille de la sous grille.
  */
  static final int SOUSGRILLESIZE = 3;
  /**
  * Array Sudoku.
  */
  static char[][] sudokuArray = new char[GRILLESIZE][GRILLESIZE];


    /** Constructeur de la classe.
    * @param sizeGrille   Taille de la grille
    */
    protected GrilleImpl(final int sizeGrille) {
      this.GRILLESIZE = sizeGrille;
      if (sizeGrille == 9) {
        this.possible = this.POSSIBLE9;
      } else if (sizeGrille == 16) {
        this.possible = this.POSSIBLE16;
      } else {
        this.possible = this.POSSIBLE25;
      }
      this.sudokuArray = new char[this.GRILLESIZE][this.GRILLESIZE];
      for (int i = 0; i < this.GRILLESIZE; i++) {
        for (int j = 0; j < this.GRILLESIZE; j++) {
           this.sudokuArray[i][j] = this.EMPTY;
        }
      }
    }

    /**
    * Affiche la grille.
    *
    */
    public final void display() {
       for (int i = 0; i < this.GRILLESIZE; i++) {
            for (int j = 0; j < this.GRILLESIZE; j++) {
                System.out.print(this.getValue(i, j) + " ");
            }
            System.out.println('\n');
        }
        System.out.println("\n\n");
    }

    /**
    * @return largeur/hauteur de la grille
    */
    public final int getDimension() {
      return this.GRILLESIZE;
    }

    /**
    * Test si une valeur est possible dans la grille par rapport a ce qu'elle.
    * contient deja
    *
    * @param x     position x dans la grille
    * @param y     position y dans la grille
    * @param value valeur a mettre dans la case
    * @return boolean si une valeur peut être insere dans la grille ou pas
    * @throw IllegalArgumentException si x ou y sont hors bornes (0-8)
    * @throw IllegalArgumentException si value n'est pas un caractere autorise
    *        ('1',...,'9',..)
    */

    public final boolean possible(final int x, final int y, final char value) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }

        if (!new String(this.possible).contains(String.valueOf(value))) {
          throw new IllegalArgumentException(
            "Value n'est pas un caractère autorise");
        }

        /**** Verification duplication par ligne / colonne */
        for (int i = 0; i < GRILLESIZE; i++) {
          if (this.sudokuArray[x][i] == value) {
            return false;
          }
        }

        for (int i = 0; i < GRILLESIZE; i++) {
          if (this.sudokuArray[i][y] == value) {
            return false;
          }
        }

        return true;
    }

   /**
   * Test si une grille est terminee.
   *
   * @return true si la grille est complete
   */
    public final boolean complete() {
        /* Creation du tableau avec les valeurs par defaut */
        for (int i = 0; i < this.GRILLESIZE; i++) {
          for (int j = 0; j < this.GRILLESIZE; j++) {
            if (this.sudokuArray[i][j] == this.EMPTY) {
              return false;
            }
          }
        }
        return true;
    }

    /**
    * Recupere une valeur de la grille.
    *
    * @param x      position x dans la grille
    * @param y      position y dans la grille
    * @return valeur dans la case x,y
    * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
    */
    public final char getValue(final int x, final int y) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        return this.sudokuArray[x][y];
    }

    /**
    * Affecte une valeur dans la grille.
    *
    * @param x       position x dans la grille
    * @param y       position y dans la grille
    * @param value
    *            valeur a mettre dans la case
    * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
    * @throws IllegalArgumentException si la valeur est interdite aux vues des
    *        autres valeurs de la grille
    * @throws IllegalArgumentException si value n'est pas un caractere autorise
    *        ('1',...,'9')
    */

    public final void setValue(final int x, final int y, final char value) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        if (!new String(this.possible).contains(String.valueOf(value))) {
          throw new IllegalArgumentException(
            "Value n'est pas un caractère autorise - " + String.valueOf(value)
            );
        }
        if ((value != '@') && (!this.possible(x, y, value))) {
          throw new IllegalArgumentException(
            "la valeur est interdite aux vues des autres valeurs de la grille"
            );
        }
        this.sudokuArray[x][y] = value;
    }
}
