import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * Classe de Parsing de fichier TXT pour construction object Grille.
 */
public final class GrilleParser {

    /**
    * Parse le fichier fournis pour construction complet de la grille.
    *
    * @param in       InputStream Fichier fournit
    * @param grille       Grille à loader par rapport au fichier
    * @throws IOException si formatage de fichier incorrect
    */
    public static void parse(final InputStream in,
        final Grille grille) throws IOException {
        Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
        int dimension = grille.getDimension();
        char[] buffer = new char[dimension];
        for (int line = 0; line < dimension; line++) {
            int lus = reader.read(buffer);
            if (lus != dimension) {
                throw new EOFException("format incorrect");
            }
            for (int i = 0; i < dimension; i++) {
                grille.setValue(line, i, buffer[i]);
            }
            lus = reader.read(new char[1]);
            if (lus != 1) {
                throw new EOFException("pas de fin de ligne ? ligne=" + line);
            }
        }
        reader.close();
    }

    /**
    * Parse le fichier fournis pour construction complet de la grille.
    *
    * @param f       File Fichier fournit
    * @param grille       Grille à loader par rapport au fichier
    * @throws IOException si formatage de fichier incorrect
    */
    public static void parse(final File f,
        final Grille grille) throws IOException {
        parse(new FileInputStream(f), grille);
    }
}
