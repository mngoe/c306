// import pour les methodes statique Assert.
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


// import pour la methode statique Test.
import org.junit.jupiter.api.*;

/**
 * Tests unitaire pour la classe Calcul.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public class GrilleImplTest {

    /**
    * Variable NUM1 entier.
    */
    public static final int NUM1 = 1;
    /**
    * Variable NUM2 entier.
    */
    public static final int NUM2 = 2;
    /**
    * Variable NUM3 entier.
    */
    public static final int NUM3 = 3;

    /**
    * Caractere de case vide
    */
    public static final char EMPTY = '@';

    /**
    * Caractere de test saisie 5
    */
    public static final char CHAR5 = '5';

    /**
    * Caractere de test saisie G
    */
    public static final char CHARG = 'g';

    /**
    * Variable NUM4 entier.
    */
    public static final int NUM4 = 4;

    /**
    * Variable NUM4 entier.
    */
    public static final int NUM18 = 18;

    /**
    * Variable NUM4 entier.
    */
    public static final int NUMCHECK1 = 16;

    public static Grille mygrille;

    /**
    * Test unitaire du Constructeur().
    */
    @BeforeAll
    public static void testConstructeur() {
        mygrille = new GrilleImpl();
    }

    /**
    * Test unitaire de la methode somme().
    */
    @Test
    public final void testGetDimension() {
        GrilleImpl testGrille = new GrilleImpl();
        assertEquals(NUMCHECK1, testGrille.getDimension());
    }


    /**
    * Test unitaire de la methode getValue().
    */
    @Test
    public void testGetValue() {
        GrilleImpl testGrille = new GrilleImpl();
        assertEquals(EMPTY, testGrille.getValue(NUM3, NUM1));
        testGrille.setValue(NUM3, NUM1,CHAR5);
        assertEquals(CHAR5, testGrille.getValue(NUM3, NUM1));
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.getValue(NUM3, NUM18);
        });
        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.getValue(NUM18, NUM3);
        });
        expectedMessage = "Parametre Hors Borne";
        actualMessage = exception2.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
    * Test unitaire de la methode setValue().
    */
    @Test
    public void testSetValue() {
        GrilleImpl testGrille = new GrilleImpl();
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.setValue(NUM3, NUM18,CHAR5);
        });

        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));



        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.setValue(NUM3, NUM1,CHARG);
        });
        expectedMessage = "Value n'est pas un caractère autorisé";
        actualMessage = exception2.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        testGrille.setValue(NUM3, NUM1,CHAR5);
        assertEquals(CHAR5, testGrille.getValue(NUM3, NUM1));

        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.setValue(NUM3, NUM2,CHAR5);
        });
        expectedMessage = "la valeur est interdite aux vues des autres valeurs de la grille";
        actualMessage = exception3.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        Throwable exception4 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.setValue(NUM18, NUM3,CHAR5);
        });

        expectedMessage = "Parametre Hors Borne";
        actualMessage = exception4.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }



    /**
    * Test unitaire de la methode possible().
    */
    @Test
    public void testPossible() {
        GrilleImpl testGrille = new GrilleImpl();
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.possible(NUM3, NUM18, CHAR5);
        });
        String expectedMessage = "Parametre Hors Borne";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.possible(NUM18, NUM3, CHAR5);
        });
        expectedMessage = "Parametre Hors Borne";
        actualMessage = exception2.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> {
            testGrille.possible(NUM3, NUM1, CHARG);
        });
        expectedMessage = "Value n'est pas un caractère autorisé";        
        actualMessage = exception3.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        testGrille.setValue(NUM3, NUM1, CHAR5);
        assertFalse(testGrille.possible(NUM3, NUM2, CHAR5));
        assertFalse(testGrille.possible(NUM2, NUM1, CHAR5));
    }



    /**
    * Test unitaire de la methode possible().
    */
    @Test
    public void testComplete() {
        GrilleImpl testGrille = new GrilleImpl();
        assertFalse(testGrille.complete());      
    }


}
