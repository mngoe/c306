/**
* Implementation classe de grille.
* @author maxime.ngoe@gmail.com (Maxime NGOE)
**/

/**
 *Implémentation Interface Grille de sudoku.
 */
public class GrilleImpl implements Grille {

  /**
  * Taille de la grille.
  */
  static final int GRILLESIZE = 16;
  /**
  * Array Sudoku.
  */
  static final char[][] SUDOKUARRAY = new char[GRILLESIZE][GRILLESIZE];


    /** Constructeur de la classe.
    */
    protected GrilleImpl() {
      for (int i = 0; i < this.GRILLESIZE; i++) {
        for (int j = 0; j < this.GRILLESIZE; j++) {
           this.SUDOKUARRAY[i][j] = this.EMPTY;
        }
      }
    }

    /**
    * @return largeur/hauteur de la grille
    */
    public final int getDimension() {
      return this.SUDOKUARRAY.length;
    }

    /**
    * Test si une valeur est possible dans la grille par rapport a ce qu'elle.
    * contient deja
    *
    * @param x     position x dans la grille
    * @param y     position y dans la grille
    * @param value valeur a mettre dans la case
    * @return boolean si une valeur peut être inséré dans la grille ou pas
    * @throw IllegalArgumentException si x ou y sont hors bornes (0-8)
    * @throw IllegalArgumentException si value n'est pas un caractere autorise
    *        ('1',...,'9',..)
    */

    public final boolean possible(final int x, final int y, final char value) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }

        if (!new String(this.POSSIBLE).contains(String.valueOf(value))) {
          throw new IllegalArgumentException(
            "Value n'est pas un caractère autorisé");
        }

        /**** Vérification duplication par ligne / colonne */
        for (int i = 0; i < GRILLESIZE; i++) {
          if (this.SUDOKUARRAY[x][i] == value) {
            return false;
          }
        }

        for (int i = 0; i < GRILLESIZE; i++) {
          if (this.SUDOKUARRAY[i][y] == value) {
            return false;
          }
        }

        return true;
    }

   /**
   * Test si une grille est terminee.
   *
   * @return true si la grille est complete
   */
    public final boolean complete() {
        /* Création du tableau avec les valeurs par défaut */
        for (int i = 0; i <= this.GRILLESIZE; i++) {
          for (int j = 0; j <= this.GRILLESIZE; j++) {
            if (this.SUDOKUARRAY[i][j] == this.EMPTY) {
              return false;
            }
          }
        }
        return true;
    }

    /**
    * Recupere une valeur de la grille.
    *
    * @param x      position x dans la grille
    * @param y      position y dans la grille
    * @return valeur dans la case x,y
    * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
    */
    public final char getValue(final int x, final int y) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        return this.SUDOKUARRAY[x][y];
    }

    /**
    * Affecte une valeur dans la grille.
    *
    * @param x       position x dans la grille
    * @param y       position y dans la grille
    * @param value
    *            valeur a mettre dans la case
    * @throws IllegalArgumentException si x ou y sont hors bornes (0-8)
    * @throws IllegalArgumentException si la valeur est interdite aux vues des
    *        autres valeurs de la grille
    * @throws IllegalArgumentException si value n'est pas un caractere autorise
    *        ('1',...,'9')
    */

    public final void setValue(final int x, final int y, final char value) {
        if (x > this.getDimension() || y > this.getDimension()) {
          throw new IllegalArgumentException("Parametre Hors Borne");
        }
        if (!new String(this.POSSIBLE).contains(String.valueOf(value))) {
          throw new IllegalArgumentException(
            "Value n'est pas un caractère autorisé"
            );
        }
        if (!this.possible(x, y, value)) {
          throw new IllegalArgumentException(
            "la valeur est interdite aux vues des autres valeurs de la grille"
            );
        }
        this.SUDOKUARRAY[x][y] = value;
    }
}
