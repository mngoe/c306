/**
* Classe de Calcul
* @author maxime.ngoe@gmail.com (Maxime NGOE)
**/
public final class Calcul {

  /**
  * Variable NUM1 entier.
  */
  public static final int NUM1 = 10;

  /** Constructeur de la classe.
  * @throw IllegalArgumentException si b == 0
  */
  protected Calcul() {
    // Empecher l'instantiation
    throw new AssertionError("Instantiating utility class...");
  }

  /** Calcul la somme de deux nombres.
  * @param a entier
  * @param b entier
  * @return la valeur de la somme des parametres
  */
  public static int somme(final int a, final int b) {
    return a + b;
  }

  /** Division de 2 nombre sur le second parametre est superieur a 10.
  * @param a entier
  * @param b entier
  * @return a/b si b > Num1 (=10)
  */
  public static int maFonction(final int a, final int b) {
   if (b >= NUM1) {
    return a / b;
   }
   return b;
  }

  /**
    * @return a / b si b != 0
    * @param a entier
    * @param b entier
    * @throw IllegalArgumentException si b == 0
    */
  public static int division(final int a, final int b) {
   if (b == 0) {
     throw new IllegalArgumentException("b ne doit pas etre 0");
   }
   return a / b;
  }
}
