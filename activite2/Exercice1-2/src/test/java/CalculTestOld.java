// import pour la methode statique Assert.
import static org.junit.jupiter.api.Assertions.assertEquals;
// import pour la methode statique Test.
import org.junit.jupiter.api.Test;

/**
 * Tests unitaire pour la classe Calcul.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public class CalculTestOld {

    /**
    * Variable NUM1 entier.
    */
    public static final int NUM1 = 2;
    /**
    * Variable NUM2 entier.
    */
    public static final int NUM2 = 3;
    /**
    * Variable NUM3 entier.
    */
    public static final int NUM3 = 8;
    /**
    * Variable NUMCHECK1 entier.
    */
    public static final int NUMCHECK1 = 5;

    /**
    * Variable NUMCHECK2 entier.
    */
    public static final int NUMCHECK2 = 4;

    /**
    * Test unitaire du Constructeur().
    */
    @Test
    public void testConstructeur() {
        new Calcul();
    }

    /**
    * Test unitaire de la methode somme().
    */
    @Test
    public void testSomme() {
        assertEquals(NUMCHECK1, Calcul.somme(NUM1, NUM2));
    }

    /**
    * Test unitaire de la methode Division().
    */
    @Test
    public void testDivision() {
        assertEquals(NUMCHECK2, Calcul.division(NUM3, NUM1));
    }

}
