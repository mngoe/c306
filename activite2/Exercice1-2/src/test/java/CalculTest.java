// import pour les methodes statique Assert.
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

// import pour la methode statique Test.
import org.junit.jupiter.api.Test;

/**
 * Tests unitaire pour la classe Calcul.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public class CalculTest {

    /**
    * Variable NUM1 entier.
    */
    public static final int NUM1 = 2;
    /**
    * Variable NUM2 entier.
    */
    public static final int NUM2 = 3;
    /**
    * Variable NUM3 entier.
    */
    public static final int NUM3 = 8;

    /**
    * Variable NUM4 entier.
    */
    public static final int NUM4 = 16;

    /**
    * Variable NUMCHECK1 entier.
    */
    public static final int NUMCHECK1 = 5;

    /**
    * Variable NUMCHECK2 entier.
    */
    public static final int NUMCHECK2 = 4;

    /**
    * Variable NUMCHECK3 entier.
    */
    public static final double NUMCHECK3 = 0.5;

    /**
    * Test unitaire du Constructeur().
    */
    @Test
    public void testConstructeur() {
        new Calcul();
    }

    /**
    * Test unitaire de la methode somme().
    */
    @Test
    public void testSomme() {
        assertEquals(NUMCHECK1, Calcul.somme(NUM1, NUM2));
    }


    /**
    * Test unitaire de la methode maFonction().
    */
    @Test
    public void testmaFonction() {
        assertEquals(NUM1, Calcul.maFonction(NUM3, NUM1));
        assertEquals(NUMCHECK3, Calcul.maFonction(NUM3, NUM4));
    }

    /**
    * Test unitaire de la methode Division().
    */
    @Test
    public void testDivision() {
        assertEquals(NUMCHECK2, Calcul.division(NUM3, NUM1));

        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            Calcul.division(NUM3, 0);
        });
 
        String expectedMessage = "b ne doit pas etre 0";
        String actualMessage = exception.getMessage();
     
        assertTrue(actualMessage.contains(expectedMessage));
    }


    /**
    * Test unitaire de la methode MaFonction().
    */
    @Test
    public void testMaFonction() {
        assertEquals(NUM3, Calcul.division(NUM3, NUM1));
        assertEquals(NUMCHECK3, Calcul.division(NUM3, NUM4));
    }
}
