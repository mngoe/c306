

""" Checkstyle Command 
java -jar checkstyle-8.36.2-all.jar -c sun_checks.xml ./activite1/Exercice1/src/main/java/Exercice1-init.java
java -jar checkstyle-8.36.2-all.jar -c sun_checks.xml ./activite1/Exercice1/src/main/java/Exercice1-correct.java
java -jar checkstyle-8.36.2-all.jar -c sun_checks.xml ./activite1/Exercice2/src/main/java/TabAlgos.java
java -jar checkstyle-8.36.2-all.jar -c sun_checks.xml ./activite1/Exercice2/src/test/java/TestTabAlgos.java


""" Compilation Java 
javac -encoding utf8 -d ./activite1/Exercice1/target  -cp  junit-platform-console-standalone-1.7.0.jar:out ./activite1/Exercice1/src/main/java/Exercice1-correct.java
javac -encoding utf8 -d ./activite1/Exercice2/target  -cp  junit-platform-console-standalone-1.7.0.jar:out ./activite1/Exercice2/src/main/java/TabAlgos.java
javac -encoding utf8 -d ./activite1/Exercice2/target  -cp  junit-platform-console-standalone-1.7.0.jar:out ./activite1/Exercice2/src/test/java/TestTabAlgos.java

""" Test Unitaire JUnit
java -jar junit-platform-console-standalone-1.7.0.jar -cp ./activite1/Exercice2/target --scan-classpath