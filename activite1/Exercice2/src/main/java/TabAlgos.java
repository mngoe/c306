/**
 * Classe TablAlgos, Exervice2.java.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public final class TabAlgos {

  private TabAlgos() {
    // Empêcher l'instantiation
    throw new AssertionError("Instantiating utility class...");
  }


  /**
   * @param tab tableau entier
   * @return valeur la plus grande d'un tableau.
   */
  public static int plusGrand(final int[] tab) {
    int i;
    int max = tab[0];
    for (i = 1; i < tab.length; i++) {
      if (tab[i] > max) {
        max = tab[i];
      }
    }
    return max;
  }

  /**
   * @param tab tableau entier
   * @return moyenne des valeurs du tableau.
   * @throw IllegalArgumentException si tab et null ou vide.
   **/
  public static double moyenne(final int[] tab) {
    int i;
    double total = 0;

    for (i = 0; i < tab.length; i++) {
      total = total + tab[i];
    }
    double moyenne = total / tab.length;
    return moyenne;
  }

  /**
   * @param tab1 tableau entier
   * @param tab2 tableau entier
   * Compare le contenu de 2 tableaux en tenant compte de l'ordre.
   * @return true si les 2 tableaux contiennent les mêmes éléments
   *         avec les mêmes nombres d'occurences
   *         (avec les elements dans le meme ordre).
   **/
  public static boolean egaux(final int[] tab1, final int[] tab2) {
    if (tab1.length != tab2.length) {
      return false;
    }

    for (int i = 0; i < tab1.length; i++) {
      if (tab1[i] != tab2[i]) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param tab1 tableau entier
   * @param tab2 tableau entier
   * Compare le contenu de 2 tableaux sans tenir compte de l'ordre.
   * @return true si les 2 tableaux contiennent les mêmes éléments
   *         avec les mêmes nombres d'occurrence
   *         (pas forcément dans le meme ordre).
   **/
  public static boolean similaires(final int[] tab1, final int[] tab2) {
    if (tab1.length != tab2.length) {
      return false;
    }

    // Tri du tableau 1
    for (int i = 0; i < tab1.length; i++) {
        for (int j = i + 1; j < tab1.length; j++) {
           if (tab1[i] > tab1[j]) {
               int temp = tab1[i];
               tab1[i] = tab1[j];
               tab1[j] = temp;
           }
        }
    }
    // Tri du tableau 2
    for (int i = 0; i < tab2.length; i++) {
        for (int j = i + 1; j < tab2.length; j++) {
           if (tab2[i] > tab2[j]) {
               int temp = tab2[i];
               tab2[i] = tab2[j];
               tab2[j] = temp;
           }
        }
    }

    // Comparaison une a une des valeurs des tableaux
    for (int i = 0; i < tab1.length; i++) {
      if (tab1[i] != tab2[i]) {
        return false;
      }
    }
    return true;
  }
}
