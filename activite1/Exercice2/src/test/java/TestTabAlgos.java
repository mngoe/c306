// import pour la méthode statique assertEquals.
import static org.junit.jupiter.api.Assertions.assertEquals;
// import pour la méthode statique assertFalse.
import static org.junit.jupiter.api.Assertions.assertFalse;
// import pour la méthode statique assertTrue.
import static org.junit.jupiter.api.Assertions.assertTrue;
// import pour l'annotation @Test indiquant qu'il s'agit d'une méthode de test.
import org.junit.jupiter.api.Test;

/**
 * Test de TablAlgos, Exervice2.java.
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public class TestTabAlgos {
    /**
    * Variable NUM1 entier.
    */
    public static final int NUM1 = 100;
    /**
    * Variable NUM2 entier.
    */
    public static final int NUM2 = 110;
    /**
    * Variable NUM3 entier.
    */
    public static final int NUM3 = 150;
    /**
    * Variable NUMCHECK entier.
    */
    public static final int NUMCHECK = 120;

    /**
    * Test unitaire de la methode plusGrand().
    */
    @Test
    public final void testPlusGrand() {
        int[] intArray = new int[] {NUM1, NUM2, NUM3};
        assertEquals(NUM3, TabAlgos.plusGrand(intArray));
    }

    /**
    * Test unitaire de la methode moyenne().
    */
    @Test
    public final void testMoyenne() {
        int[] intArray = new int[] {NUM1, NUM2, NUM3};
        assertEquals(NUMCHECK, TabAlgos.moyenne(intArray));
    }


    /**
    * Test unitaire de la methode egaux().
    */
    @Test
    public final void testEgaux() {
        int[] intArray = new int[] {NUM1, NUM2, NUM3};
        int[] intArray2 = new int[] {NUM3, NUM2, NUM1};
        int[] intArray3 = new int[] {NUM3, NUM2};
        assertTrue(TabAlgos.egaux(intArray, intArray));
        assertFalse(TabAlgos.egaux(intArray, intArray2));
        assertFalse(TabAlgos.egaux(intArray, intArray3));
    }


    /**
    * Test unitaire de la methode similaires().
    */
    @Test
    public final void similaires() {
        int[] intArray = new int[] {NUM1, NUM2, NUM3, NUM1};
        int[] intArray2 = new int[] {NUM3, NUM2, NUM1, NUM1};
        int[] intArray3 = new int[] {NUM3, NUM2};
        int[] intArray4 = new int[] {NUM1, NUM2, NUM3, NUM3};
        assertTrue(TabAlgos.similaires(intArray, intArray));
        assertTrue(TabAlgos.similaires(intArray, intArray2));
        assertFalse(TabAlgos.similaires(intArray, intArray3));
        assertFalse(TabAlgos.similaires(intArray, intArray4));
    }
}
