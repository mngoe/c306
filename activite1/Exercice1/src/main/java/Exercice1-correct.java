/**
 * Test d'ecriture, Exervice1-correct.java .
 *
 * @author maxime.ngoe@gmail.com (Maxime NGOE)
 */
public final class Exemple {

 /**
 * La chaine de caractère t.
 */
 private final String t;

 /**
 * Variable v entier.
 */
 private final int v;

 /**
 * @param texemple String
 */
 public Exemple(final String texemple) {
  t = texemple;
 }

 /**
 * @return la valeur de v
 */
 public int getV() {
  return this.v;
 }

 /**
 * Definit la valeur de v.
 * @param vexemple entier
 */
 public void setV(final int vexemple) {
  this.v = vexemple;
 }

 /**
 * @return t si v est positif
 */
 public String getT() {
  if (v > 0) {
   return t;
  }
 }
}
